$('.single-item').slick({
   arrows: false,
   dots: true,
   autoplay: true
});
$('.autoplay').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    arrows: true,
    prevArrow:'<svg class="slick-prev pull-left slick-arrow"><img style="width: 20px; height: 20px; position: relative; top: 45px; right: 30px; z-index:1;" src="img/angle-left-solid.svg" alt="logo"></svg>',
    nextArrow: '<svg class="slick-next pull-right slick-arrow"><img style="width: 20px; height: 20px; position:relative; left: 101%; bottom:45px" src="img/angle-right-solid.svg" alt="logo"></svg>',
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 3,
                slidesToSroll: 1,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }
    ]
  });
      