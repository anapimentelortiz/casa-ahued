﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/public/Site1.Master" CodeBehind="empresa.aspx.vb" Inherits="casa_ahued.empresa" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="empresa">
        <div class="row">
            <div class="col-lg-5 image"><img src="img/empresa/historia.jpg" alt="historia" /></div>
            <div class="col-lg-7 text">
                <h1>HISTORIA</h1>
                <p>
                    Casa Ahued es una empresa que a lo largo de los años se ha consolidado como una de las empresas líder en su ramo, prueba de que el esfuerzo, la perseverancia y el trabajo constante dan frutos.
                </p>
                <p>
                    Comienza como un negocio familiar en la ciudad de Xalapa, Veracruz teniendo sus antecedentes desde el año 1965. Iniciándose como un negocio de artículos para el hogar, ubicando su primer tienda
                    en la esquina que forman las calles de Juárez y Revolución.
                </p>
                <p>
                   En 1988, se reubican a un local comercial más amplio, en el centro de esta ciudad y dan empleo a 5 personas, las cuales apoyaban en las diversas actividades de la empresa. Años más tarde y debido
                   al éxito que estaban teniendo, se proyecta una sucursal para poder abastecer la creciente demanda de la clientela, dicha sucursal se inaugura en 1993 empleando a 10 personas más
                </p>
                <p>
                    El crecimiento siguió, y ese mismo año, se apertura una bodega central donde se recibe toda la mercancía para luego ser enviada a las tiendas.
                </p>
                <p>
                    Ya para el año 1999, se abre una segunda sucursal, donde también laboraban 5 personas, sin embargo, al no contar con un espacio adecuado y ante la notable necesidad de atención a la población que
                    demandaba los productos, se inaugura un local cuyas características permitieran la exhibición de la mercancía y en donde el cliente se sintiera libre y a gusto de poder realizar sus compras.
                </p>
            </div>
        </div>
        <div class="row dos">
             <div class="col-lg-7 text">
                <p>
                    Un año más tarde, en el 2000 y con una empresa más sólida, tienen la satisfacción de abrir su tercera sucursal, Clavijero 40, la cual lleva el mismo nombre que su dirección. Sus trabajadores,
                    ya no eran solo 5 o 10, sino 55, teniendo 1 gerente, 1 subgerente, 3 supervisores y 50 empleados de piso, distribuidos en 10 departamentos, sin embargo, la demanda que se tenía era cada vez 
                    mayor, ya que la venta se extendía a poblaciones cercanas a la ciudad de Xalapa y también fuera de ésta. Es esa razón la que impulsa el diseño de una cuarta sucursal, pero para ello, se toma
                    la decisión de cerrar una de las otras tres sucursales.
                </p>
                <p>
                    Llegó diciembre de 2004, y para el abastecimiento de la temporada decembrina, se apertura la sucursal de Melchor Ocampo Esquina Clavijero en donde el equipo de trabajo lo formaban 21 personas,
                    1 gerente, 1 subgerente, 1 supervisor y 18 trabajadores que laboraban en el área de ventas.
                </p>
                <p>
                    El 28 de octubre del año 2006, de la misma manera que hace dos años, se pretende manejar a un ritmo más elevado el sistema de importación y juguetería y se da inicio a la sucursal de Clavijero 38
                    ubicada en la dirección del nombre de la misma, sucursal en la cual se pudieran albergar productos de acuerdo a la temporada del año. Ésta abre sus puertas contando con 10 trabajadores: 1 gerente,
                    1 supervisor y 8 empleados en piso de venta.
                </p>
                <p>
                    Considerando la visión de expansión del Director General y en atención a la demanda de la población femenil, en el 2007 se abre la sucursal “Look” ubicaba en Juárez No. 86 en donde el giro del producto
                    fue completamente pensado en el público femenino, atendido por 5 trabajadoras: 1 gerente, 1 supervisor y 3 trabajadoras de venta.
                </p>
                <p>
                    En ese mismo año, en el mes de diciembre, se decide impulsar la sucursal de Revolución, remodelándola para lanzar al público artículos de juguetería. Aunado a ello, se renueva la imagen de la empresa y 
                    se decide cerrar la sucursal de Lucio a principios del 2008, para dar comienzo a la ampliación de nuevos espacios ya no solo en la ciudad de Xalapa sino fuera de ella.
                </p>
                <p>
                    A lo largo de estos 50 años, hemos distribuido productos para el hogar, como son: loza, peltre, plásticos, lámina, cristalería, cuchillería, melanina, artículos navideños y de temporada, de importación,
                    y teniendo ventas tanto al mayoreo, medio mayoreo como al menudeo, en los estados de Veracruz, Puebla y Oaxaca, satisfaciendo la creciente necesidad de la población, ofreciéndole a nuestra amable clientela
                    los mejor de nosotros. Visítenos y compruebe que como Casa Ahued no hay dos.
                </p>
            </div>
            <div class="col-lg-5 image"><img src="img/empresa/historia-01.jpg" alt="historia" /></div>
        </div>
        <div class="row">
            <div class="col-lg-5 mision-title"><h1>MISIÓN</h1></div>
            <div class="col-lg-7 mision-text">
                <p>
                    Casa Ahued es un organismo social dedicado a ofrecer artículos para el hogar, productos de importación y juguetería, con el fin de consolidarse en el mercado como una empresa con excelente aceptación, con valores y políticas fincadas
                    en compromiso social, integrada al desarrollo económico de la región y comprometida de manera constante con nuestro equipo de trabajo, para generar un crecimiento integral que fomente un ambiente laboral cordial, brindando a los clientes
                    una atención personalizada y garantizando la calidad de los productos, al mejor precio.
                </p>
            </div>
        </div>
        <div class="row">
              <div class="col-lg-7 vision-text">
                <p>
                    Consolidarse como una empresa líder, eficiente y preparada para la modernización, incorporando al mercado productos de calidad, contando con un equipo de trabajo que proyecte
                    un compromiso y espíritu de servicio, con el cliente, y reflejen honradez y disciplina hacia la empresa, mediante la evaluación constante de los procesos que implican tanto las
                    fortalezas como las debilidades que se generan como parte de nuestra misión.
                </p>
            </div>
            <div class="col-lg-5 vision-title"><h1>VISIÓN</h1></div>
        </div>
        
        <h1 class="valores">VALORES</h1>
        <div class="row">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col end">
                         <h4>HONESTIDAD:</h4>
                        <p>
                          Como empresa promovemos la verdas como herramienta para generar confianza y la credibilidad.
                        </p>
                    </div>
                    <img class="iconos" src="img/empresa/honestidad.png" alt="honestidad" />
                </div>
            </div>
            <div class="col-lg-6 start">
                 <div class="row">
                      <img class="iconos" src="img/empresa/seguridad.png" alt="seguridad" />
                    <div class="col">
                         <h4>SEGURIDAD:</h4>
                        <p>
                            Hacemos referencia al vínculo de confianza con nuestros clientes.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row fila">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col end">
                         <h4>CALIDAD:</h4>
                        <p>
                            Los productos que ofrecemos, se espera que cumplan con ciertos parámetros, los cuales deben estar lo más cerca
                            posible de la excelencia.
                        </p>
                    </div>
                    <img class="iconos" src="img/empresa/calidad.png" alt="calidad" />
                </div>
            </div>
            <div class="col-lg-6 start">
                 <div class="row">
                      <img class="iconos" src="img/empresa/responsabilidad.png" alt="responsabilidad" />
                    <div class="col">
                         <h4>RESPONSABILIDAD:</h4>
                        <p>
                            Tratamos de tener un nivel alto de compromiso tanto con los trabajadores como con los clientes.
                            Con los trabajadores la empresa se compromette a brindar estabilidad y excelentes condiciones laborales.
                            Para con los clientes, la empresa se compromete a producir bienes y servicios de calidad.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
