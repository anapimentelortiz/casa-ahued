﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/public/Site1.Master" CodeBehind="jugueteria.aspx.vb" Inherits="casa_ahued.jugueteria" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <section id="banner-jugueteria">
        <img src="img/jugueteria/banner.jpg" alt="banner">
    </section>
    <section class="container-fluid" id="jugueteria">
        <h1>JUEGOS DE CONSTRUCCIÓN CON IMANES</h1>
        <div class="row">
            <div class="col-md-12 col-lg-6 image">
                <img src="img/jugueteria/juguetes-01.jpg" alt="juguetes"/>
            </div>
            <div class="col-md-12 col-lg-6 text">
                <p>
                    Los juegos de construcción ayudan a los niños y niñas a experimentar con el espacio y estimulan la imaginación. Hay
                    muchos tipos de juegos de construcción, que les permite disfrutar de la aventura de crear vehículos y construcciones
                    variadas con bloques o piezas que encajan unas con otras.
                </p>
                <p>
                    Los juegos de construcción más comunes son los de bloques como Lego, pero hay muchos más juegos de construcción con
                    imanes en los extremos y, ocasionalmente, en otros puntos de su estructura para poder unirlos y hacer distintas formas.
                </p>
                <h5>JUEGOS DE CONSTRUCCIÓN PARA DISFRUTAR CREANDO</h5>
                <p>Los juegos de construcción con imanes pueden transformar estructuras ya prediseñadas, pero las piezas pueden utlizarse para
                    crear otras formas distintas a las que se propone en los packs a la venta. Todas las piezas están preparadas para poder
                    ser utilizadas para crear los diseños que se quiera.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-6 order-md-1 order-lg-0 text">
                <p>Solo hay que utilizar el imán situado en los extremos de las piezas, para encontrar la forma que se quiere obtener. Además,
                   según los tipos de juego de construcción, hay piezas distintas que pueden utilizarse para crear distintas formas y construcciones.
                   Solo hay que elegir entre la gran variedad de ese tipo de juegos de construcción con imanes para poder construir formas geométricas,
                   vehículos o dificios que encenderán su imaginación.
                </p>
                <h5>JUEGOS CON IMANES PARA APRENDER</h5>
                <p>
                    La ventaja de los juegos de construcción con imanes es que no solo puede ser un juego muy divertido y entretenido, sino que además ofrece
                    la posibilidad de aprender. La función de los imanes, cómo funcionan, qué aplicaciones pueden tener, cómo se puede construir una figura
                    con ellos y muchas más cosas que harán que puedan pasar horas y horas divirtiéndose con estos juegos para crear edificios, formas geométricas,
                    vehículos y mucho más, sin más límite que su imaginación.
                </p>
                <p>
                    Descubre la variedad de juegos de construcción con imanes que hay en el catálago de juguetes online de Todojuguete. Un juego divertido, original
                    y diferente que puede ser un gran regalo para Navidad, Reyes Magos y en cualquier ocasión.
                </p>
            </div>
            <div class="col-md-12 col-lg-6 order-md-0 order-lg-1 image">
                 <img src="img/jugueteria/juguetes-02.jpg" alt="juguetes"/>
            </div>
        </div>
    </section>
</asp:Content>
