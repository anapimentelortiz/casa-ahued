﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/public/Site1.Master" CodeBehind="tendencias.aspx.vb" Inherits="casa_ahued.tendencias" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="banner-tendencias">
        <img src="img/tendencias/banner.jpg" alt="tendencia">
    </section>
     <section id="tendencias">
        <h1>LOS PLATOS UN ELEMENTO DECORATIVO</h1>
        <div class="row">
            <div class="col-md-12 col-lg-6 image">
                <img src="img/tendencias/tendencias-01.png" alt="tendencia"/>
            </div>
            <div class="col-md-12 col-lg-6 text">
                <h2>CÓMO ELEGIR Y COMPRAR VAJILLA DE DIARIO</h2>
                <p>
                   Los platos se reinventan como el elemento decorativo de moda, decorar las paredes con platos fue una tendencia hace muchos
                    años, y quizá haya a quien le suene anticuado. Sin embargo, las tendencias decorativas están recuperando este elemento y
                    adaptándolo a los estilos modernos.
                </p>
                <p>
                    Utilizar vajilla y cerámica colgada de las paredes para decorar interiores es tendencia.
                    En otras ocasiones hemos hablado de cómo crear conjuntos decorativos enteros con un solo un elemento,
                    como los espejos o los libros, y en esta ocasión le toca el turno a un objeto que se está reinventando como
                    medio de moda para la ilustración, el diseño y el interiorismo: el plato.
                </p>
                <p>
                    Los platos pueden ser, de por sí, elementos muy bonitos, decorados con todo tipo de imágenes, colores y texturas; pero si encima
                    los combinamos y disponemos de formas creativas, pueden transformar por el completo el aspecto de una estancia. La versatibilidad
                    que ofrecen en cuanto a tamaños, diseños, tonos y texturas; nos permiten innovar a nuestro gusto con la forma en que organizamos
                    y planteamos las composiciones.
                </p>
            </div>
        </div>
        <div class="row infor">
            <div class="col-lg-4 text">
                <p>
                    La clave está en adaptar esta decoración para que sea vintage en lugar de anticuada, y kitsch en lugar de rococó. Es decir,
                    aprovechar la inspiración clásica y las reminiscencias retro o rurales que pueden aportar los platos pero en armonía con un estilo
                    más moderno y renovado. Las paredes decoradas con platos encajan a la perfección dentro del estilo shabby chic, si combinamos
                    modelos antiguos con otros de diseño contemporáneo; con el vintage, si el conjunto de la decoración del cuarto también sigue estética;
                    y si empleamos tonos pastel dotaremos a cualquier habitación de un dulce aire naïf.
                </p>
            </div>
            <div class="col">
                <img src="img/tendencias/tendencias-02.png" alt="tendencia" />
            </div>
        </div>
        <div class="row pb-3">
            <div class="col-lg-6">
                <img src="img/tendencias/tendencias-03.png" alt="image" />
            </div>
            <div class="col-lg-6">
                <div class="row img3"><img src="img/tendencias/tendencias-04.png" alt="image" /></div>
                <div class="row"><img src="img/tendencias/tendencias-05.png" alt="image" /></div>
            </div>
        </div>
    </section>
</asp:Content>
