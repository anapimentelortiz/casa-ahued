﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/public/Site1.Master" CodeBehind="sucursales.aspx.vb" Inherits="casa_ahued.sucursales" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="container-fluid" id="casa-ahued">
        <h1>Casa Ahued</h1>
        <div class="row mb-5">

            <div class="col">
                <h3>Clavijero 40 y 42</h3>
                <div class="row">
                    <div class="col-lg-6">
                        <img src="img/sucursales/casa-ahued/clavijero40.jpg" alt="Clavijero" />
                    </div>
                    <div class="col-lg-6">
                        <p>
                            Av. Clavijero No. 40 y 42 <br />
                            Col. Centro entre Altamirano y Victoria, Xalapa, Ver. <br />
                            Teléfonos: (228)841-1677, (228)8411678, (228)8411679
                        </p>
                        <div class="row">
                           <iframe title="mapa1" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1880.1270445093714!2d-96.924446!3d19.530702!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x87dfe53e80023fcd!2sCasa%20Ahued!5e0!3m2!1ses-419!2sus!4v1577131133008!5m2!1ses-419!2sus" style="border:0;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <h3>Revolución 149</h3>
                <div class="row two">
                    <div class="col-lg-6">
                        <img src="img/sucursales/casa-ahued/revolucion149.jpg" alt="Clavijero" />
                    </div>
                    <div class="col-lg-6">
                        <p>
                            Av. Revolución No. 149 <br />
                            Col. Centro entre Altamirano y Poeta, Xalapa, Ver.<br />
                            Teléfonos: (228)818-5652, (228)841-1681
                        </p>
                        <div class="row">
                            <iframe title="mapa2" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1880.1107479712887!2d-96.92364!3d19.532102!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x19c7c4017bc6a33e!2sCasa%20Ahued!5e0!3m2!1ses-419!2smx!4v1577134976263!5m2!1ses-419!2smx" style="border:0;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row mb-5">

            <div class="col">
                <h3>Pradera</h3>
                <div class="row">
                    <div class="col-lg-6">
                        <img src="img/sucursales/casa-ahued/pradera.jpg" alt="Pradera" />
                    </div>
                    <div class="col-lg-6">
                        <p>
                            Plaza la pradera <br />
                            Local 1A kilómetro 1125 Carretera Las Trancas, Xalapa, Ver. <br />
                            Teléfono: (228)817-0308
                        </p>
                        <div class="row">
                           <iframe title="mapa3" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3760.910733047821!2d-96.859613!3d19.502476!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x56a829567bbcb377!2sCasa%20Ahued!5e0!3m2!1ses-419!2sus!4v1577136056218!5m2!1ses-419!2sus" style="border:0;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <h3>Perote</h3>
                <div class="row two">
                    <div class="col-lg-6">
                        <img src="img/sucursales/casa-ahued/perote.jpg" alt="Perote" />
                    </div>
                    <div class="col-lg-6">
                        <p>
                            General Manuel Rincón No. 101 <br />
                            Col. Centro, Perote, Ver. <br />
                            Teléfono: (228)825-2924
                        </p>
                        <div class="row">
                            <iframe title="mapa4" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1880.1107479712887!2d-96.92364!3d19.532102!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x19c7c4017bc6a33e!2sCasa%20Ahued!5e0!3m2!1ses-419!2smx!4v1577134976263!5m2!1ses-419!2smx" style="border:0;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row mb-5">

            <div class="col">
                <h3>Bodega Animas</h3>
                <div class="row">
                    <div class="col-lg-6">
                        <img src="img/sucursales/casa-ahued/animas.jpg" alt="Animas" />
                    </div>
                    <div class="col-lg-6">
                        <p>
                            Chignautla 1 Esq. Lázaro Cárdenas <br />
                            Acueducto Animas, Xalapa, Ver. CP. 91193 <br />
                            Teléfono: 
                        </p>
                        <div class="row">
                           <iframe title="mapa5" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3760.4156942203435!2d-96.895308!3d19.523759!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x163e3e72c38b5d7e!2sBodega%20Casa%20Ahued!5e0!3m2!1ses-419!2smx!4v1577137577124!5m2!1ses-419!2smx" style="border:0;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <h3>Bodega Pico Orizaba 7</h3>
                <div class="row two">
                    <div class="col-lg-6">
                        <img src="img/sucursales/casa-ahued/pico7.jpg" alt="Pico" />
                    </div>
                    <div class="col-lg-6">
                        <p>
                            Pico de Orizaba 7 <br />
                            Casi Esq. Chignautla Inmecafe Col. Sahop, Xalapa, Ver. <br />
                            Teléfono: 
                        </p>
                        <div class="row">
                            <iframe title="map6" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3760.4216052749307!2d-96.894997!3d19.523505000000004!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85db3223ea94986f%3A0x2d0c3d6960f529d9!2sPico%20de%20Orizaba%207%2C%20Sipeh%20Animas%2C%2091190%20Xalapa-Enr%C3%ADquez%2C%20Ver.!5e0!3m2!1ses-419!2smx!4v1577137756297!5m2!1ses-419!2smx" style="border:0;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <h1>Casa Ahued Importaciones</h1>
        <div class="row mb-5">

            <div class="col">
                <h3>Clavijero 38</h3>
                <div class="row">
                    <div class="col-lg-6">
                        <img src="img/sucursales/importaciones/clavijero38.jpg" alt="Clavijero38" />
                    </div>
                    <div class="col-lg-6">
                        <p>
                           Av. Clavijero No. 38 <br />
                           Entre Altamirano y Vistoria Col. Centro, Xalapa, Ver.<br />
                           Teléfono: (228)841-1677, (228)841-1678, (228)841-1679
                        </p>
                        <div class="row">
                           <iframe title="mapa7" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1880.1270445093714!2d-96.924446!3d19.530702!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x87dfe53e80023fcd!2sCasa%20Ahued!5e0!3m2!1ses-419!2sus!4v1577139040047!5m2!1ses-419!2sus" style="border:0;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <h3>Revolución 145</h3>
                <div class="row two">
                    <div class="col-lg-6">
                        <img src="img/sucursales/importaciones/revolucion145.jpg" alt="Revolución145" />
                    </div>
                    <div class="col-lg-6">
                        <p>
                            Av. Revolución No. 145<br />
                            Entre Altamirano y Poeta Col. Centro, Xalapa, Ver. <br />
                            Teléfono: (228)817-5335
                        </p>
                        <div class="row">
                            <iframe title="mapa8" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1880.1107479712887!2d-96.92364!3d19.532102!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x19c7c4017bc6a33e!2sCasa%20Ahued!5e0!3m2!1ses-419!2smx!4v1577139299450!5m2!1ses-419!2smx" style="border:0;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row mb-5">

            <div class="col">
                <h3>Ocampo</h3>
                <div class="row">
                    <div class="col-lg-6">
                        <img src="img/sucursales/importaciones/ocampo.jpg" alt="Ocampo" />
                    </div>
                    <div class="col-lg-6">
                        <p>
                           Melchor Ocampo 20 <br />
                           Entre Revolución y Clavijero Col. Centro, Xalapa, Ver.<br />
                           Teléfono: (228)815-1757
                        </p>
                        <div class="row">
                           <iframe title="mapa9" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d940.0635222546857!2d-96.924446!3d19.530702!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x87dfe53e80023fcd!2sCasa%20Ahued!5e0!3m2!1ses-419!2smx!4v1577139663506!5m2!1ses-419!2smx" style="border:0;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <h3>Poeta</h3>
                <div class="row two">
                    <div class="col-lg-6">
                        <img src="img/sucursales/importaciones/poeta.jpg" alt="Poeta" />
                    </div>
                    <div class="col-lg-6">
                        <p>
                            Poeta Jesús Díaz 303<br />
                            Entre Francisco I. Madero y Alfaro Col. Centro, Xalapa, Ver. <br />
                            Teléfono: (228) 8151757
                        </p>
                        <div class="row">
                            <iframe title="mapa10" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1880.1005620649566!2d-96.920789!3d19.532977!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85db2e00227ee71b%3A0x43a8180d2a483336!2sCalle%20Poeta%20Jes%C3%BAs%20D%C3%ADaz%2061%2C%20Zona%20Centro%2C%20Centro%2C%2091000%20Xalapa-Enr%C3%ADquez%2C%20Ver.!5e0!3m2!1ses-419!2smx!4v1577139801154!5m2!1ses-419!2smx" style="border:0;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <h1>LOOK</h1>
        <div class="row mb-5">

            <div class="col">
                <h3>Plaza Amércias</h3>
                <div class="row">
                    <div class="col-lg-6">
                        <img src="img/sucursales/look/americas.jpg" alt="Américas" />
                    </div>
                    <div class="col-lg-6">
                        <p>
                           Plaza Américas <br />
                           Zona C. Local 32, Xalapa, Ver.<br />
                           Teléfono: (228)815-1757
                        </p>
                        <div class="row">
                           <iframe title="mapa11" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d940.1700356450297!2d-96.87735168554632!3d19.512392456483184!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85db33cb2717bb77%3A0x489f7f2b95557e6!2sCarr.+Xalapa+Veracruz+680%2C+Centro%2C+91193+Xalapa+Enr%C3%ADquez%2C+Ver.!5e0!3m2!1sen!2smx!4v1467252499324" style="border:0;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <h3>Pradera</h3>
                <div class="row two">
                    <div class="col-lg-6">
                        <img src="img/sucursales/look/pradera.jpg" alt="Pradera" />
                    </div>
                    <div class="col-lg-6">
                        <p>
                            Plaza Pradera<br />
                            Local 1 Kilómetro 1125 Carretera Las Trancas, Xalapa, Ver. <br />
                            Teléfono: (228) 815-1757
                        </p>
                        <div class="row">
                            <iframe title="mapa12" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1880.4553665239105!2d-96.859613!3d19.502476!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x56a829567bbcb377!2sCasa%20Ahued!5e0!3m2!1ses-419!2sus!4v1577140489846!5m2!1ses-419!2sus" style="border:0;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row mb-5">

            <div class="col">
                <h3>Juárez</h3>
                <div class="row">
                    <div class="col-lg-6">
                        <img src="img/sucursales/look/juarez.jpg" alt="juarez" />
                    </div>
                    <div class="col-lg-6">
                        <p>
                           Benito Juárez 88 <br />
                           Entre Clavijero y Revolución Col. Centro, Xalapa, Ver.<br />
                           Teléfono: (228)815-1757
                        </p>
                        <div class="row">
                           <iframe title="mapa13" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d940.0722577396738!2d-96.924204!3d19.529201000000004!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5b6e10592161877a!2sLook%20Accesorios!5e0!3m2!1ses-419!2smx!4v1577140958850!5m2!1ses-419!2smx" style="border:0;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <h3>Primo Verdad</h3>
                <div class="row two">
                    <div class="col-lg-6">
                        <img src="img/sucursales/look/primo.jpg" alt="primo" />
                    </div>
                    <div class="col-lg-6">
                        <p>
                            Primo Verdad<br />
                           Entre Amora e Hidalgo, Xalapa, Ver. <br />
                            Teléfono: (228) 817-0308
                        </p>
                        <div class="row">
                           <iframe title="mapa14" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15041.330609533203!2d-96.92066!3d19.527327!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe5e07888bcaebdf0!2sLook%20Accesorios!5e0!3m2!1ses-419!2smx!4v1577141123105!5m2!1ses-419!2smx" style="border:0;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</asp:Content>
