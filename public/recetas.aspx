﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/public/Site1.Master" CodeBehind="recetas.aspx.vb" Inherits="casa_ahued.recetas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="banner-recetas">
        <img src="img/recetas/banner.jpg" alt="banner">
    </section>
    <section class="container-fluid" id="receta">
        <h1>RECETAS</h1>
        <div class="row">
            <div class="col-md-12 col-lg-6 image">
                <img src="img/recetas/receta1.jpg" alt="receta"/>
            </div>
            <div class="col-md-12 col-lg-6 text">
                <h2>PREPARA LA REINA DE LAS TARTAS ESTE 'FINDE'</h2>
                <p>
                    La de queso es una de las tartas que más entusiasma en todo el mundo. En numerosos países tiene sus recetas,
                    pero básicamente hay dos formas de elaborarlas: con o sin horno. Te explicamos las diferencias entre ambas y
                    te proponemos 5 maneras de prepararlas.
                </p>
                <p>
                    Te damos a elegir entre 5 recetas entre las que hay dos opciones. Elige la que más te guste y aprovecha este fin
                    de semana para prepararla y compartirla con familia y amigos. ¡Te querrán un poco más!
                    Paso a paso: Mini tarta de queson al horno <br />
                    Una de las recetas más clásicas, en este caso de horno, a la que añadimos leche condensada como edulcolorante y dos
                    tipos de queso.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-6 order-md-1 order-lg-0 text">
                <h2>PASO A PASO</h2>
                <h5>INGREDIENTES</h5>
                <ul>
                    <li>150 g queso crema</li>
                    <li>150 g queso brie</li>
                    <li>3 ud huevos</li>
                    <li>4 cs harina</li>
                    <li>4 cs leche condensada</li>
                    <li>1 cc extracto de vainilla</li>
                    <li>Mantequilla</li>
                    <li>Grosellas rojas</li>
                    <li>Ralladura de limón</li>
                </ul>
                <h5>PREPARACIÓN</h5>
                <ul>
                    <li>Prepara el horno a 180°C.</li>
                    <li>Mezcla los huevos con los dos tipos de queso y la harina e integra bien con una varilla hasta que quede uniforme.</li>
                    <li>Añade la leche condensada, la ralladura de limón y la vainilla y mezcla bien.</li>
                    <li>Engrasa un molde redondo para 2 con la mantequilla y vuelca la mezcla.</li>
                    <li>Hornea entre 30 y 40 minutos.</li>
                    <li>Desmolda, deja que se enfrie y sirve con unas galletas rojas.</li>
                </ul>
            </div>
            <div class="col-md-12 col-lg-6 order-md-0 order-lg-1 image">
                 <img src="img/recetas/receta1.jpg" alt="receta"/>
            </div>
        </div>
    </section>
</asp:Content>
