﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/public/Site1.Master" CodeBehind="cocina.aspx.vb" Inherits="casa_ahued.cocina" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <section id="banner-cocina">
        <img src="img/cocina/banner.jpg" alt="banner">
    </section>
    <section id="cocina">
        <h1>DALE ESTILO A TU COCINA</h1>
        <div class="row">
            <div class="col-md-12 col-lg-6 image">
                <img src="img/cocina/cocina-01.png" alt="cocina"/>
            </div>
            <div class="col-md-12 col-lg-6 text">
                <h2>CÓMO ELEGIR Y COMPRAR VAJILLA DE DIARIO</h2>
                <p>
                    Cuando te enfrentas a la situación de comprar vajilla de diario te das cuenta de que no es una tarea fácil.
                    Existen miles de opciones distintas en el mercado, por lo que elegir la vajilla adecuada para tu hogar puede llegar
                    a ser una labor abrumadora. La decisión que tomes es importante, pues la vajilla es uno de los artículos de menaje del hogar
                    que vas a utilizar diariamente durante años.
                </p>
                <p>
                    Para que esa elección no sea tan complicada, a continuación te ofrecemos algunas consideraciones clave que debes
                    tener en cuenta a la hora de seleccionar tu próxima vajilla de diario. 
                </p>
                <h2>CONSEJOS ÚTILES AL COMPRAR VAJILLA NUEVA:</h2>
                <p>
                   <b class="orange">1. Toma medidas antes de comprar</b><br />
                    Ten en cuenta las dimensiones de los armarios donde vas a almacenar la vajilla nueva y el de la mesa donde la vas a utilizar.
                    También hay que considerar el tamaño y la altura del lavavajillas. <br />
                    Los platos grandes son muy bonitos, pero a veces nada prácticos (sobre todo, cuando hablamos de una vajilla de diario).
                    En muchas ocasiones no permiten cerrar las puertas de los armarios. Y cuando se meten en el lavavajillas, son tan altos que no permiten
                    que el ventilador gire sin golpearlo.
                </p>
            </div>
        </div>
        <div class="row infor">
            <div class="col-lg-4 text">
                <p>
                    <b class="orange">2. Busca vajillas por piezas sueltas</b><br />
                    Una vajilla por piezas sueltas permite personalizarla según tus necesidades. Por ejemplo, no necesitas
                    comprar una vajilla completa de 5 piezas si solo te interesan los platos llanos y los platos hondos. Las
                    vajillas con piezas sueltas te permiten seleccionar y elegir exactamente lo que deseas. Así, el desembolso
                    económico inicial será menor. Podrás ir adquiriendo más piezas a tu vajilla según lo necesites, escalonando el gasto.
                    Comprar vajilla por piezas sueltas hace también más fácil reemplazar una pieza si se rompe.
                </p>
            </div>
            <div class="col">
                <img src="img/cocina/cocina-02.png" alt="cocina" />
            </div>
        </div>
        <div class="row pb-3">
            <div class="col-lg-6">
                <img src="img/cocina/cocina-03.png" alt="image" />
            </div>
            <div class="col-lg-6">
                <div class="row img3"><img src="img/cocina/cocina-04.png" alt="image" /></div>
                <div class="row"><img src="img/cocina/cocina-05.png" alt="image" /></div>
            </div>
        </div>
    </section>
</asp:Content>
